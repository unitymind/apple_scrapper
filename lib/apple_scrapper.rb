require 'apple_scrapper/version'
require 'apple_scrapper/warranty_info'

require 'capybara'
require 'capybara/poltergeist'

module AppleScrapper
  class InvalidSerialNumber < Exception; end

  extend Capybara::DSL
  Capybara.default_driver = :poltergeist

  def self.capture_warranty(identity, proxies = nil)
    unless proxies.nil?
      Capybara.register_driver :proxied_poltergeist do |app|
        Capybara::Poltergeist::Driver.new(app, phantomjs_options: ["--proxy=#{proxies.shuffle[0]}"])
      end
      Capybara.current_driver = :proxied_poltergeist
    end

    begin
      # Submit form
      visit 'https://selfsolve.apple.com/agreementWarrantyDynamic.do'
      fill_in 'sn', with: identity
      click_button 'Continue'

      # Check for invalid serial/imei
      if has_selector? '#errorDiv'
        error_text = find('#error').text

        # Sometimes got unable to complete with valid serial/IMEI
        if error_text == "We're sorry, but we are unable to complete your request at this time. Please try again later."
          self.capture_warranty_by_identity(identity)
        else
          raise InvalidSerialNumber.new(page.find('#error').text)
        end
      end

      # Extract data
      warranty_info = AppleScrapper::WarrantyInfo.new

      warranty_info.product_name = find('#productname').text

      warranty_info.validated_purchase = true if page.has_selector?('#registration-true')
      warranty_info.validated_purchase = false if page.has_selector?('#registration-false')

      if page.has_selector?('#phone-false')
        warranty_info.phone_support_active = false
        warranty_info.phone_support_capability = find('#phone-text').text
      end

      if page.has_selector?('#phone-true')
        warranty_info.phone_support_active = true
        warranty_info.phone_support_capability, expired, _ = page.evaluate_script("document.getElementById('phone-text').innerHTML").split('<br>')
        warranty_info.phone_support_expiration_date = Date.parse(expired.sub('Estimated Expiration Date:', ''))
      end

      warranty_info.phone_support_kind = 'Apple Advisor' if warranty_info.phone_support_capability =~ /Apple Advisor/
      warranty_info.phone_support_kind = 'AppleCare+' if warranty_info.phone_support_capability =~ /AppleCare\+/


      if page.has_selector?('#hardware-true')
        warranty_info.repairs_and_service_active = true
        warranty_info.repairs_and_service_capability, expired,_ = page.evaluate_script("document.getElementById('hardware-text').innerHTML").split('<br>')
        warranty_info.repairs_and_service_expiration_date = Date.parse(expired.sub('Estimated Expiration Date:', ''))
      end

      if page.has_selector?('#hardware-false')
        warranty_info.repairs_and_service_active = false
        warranty_info.repairs_and_service_capability = find('#hardware-text').text
      end

      warranty_info.repairs_and_service_kind = 'Not Covered' if warranty_info.repairs_and_service_capability =~ /not covered/
      warranty_info.repairs_and_service_kind = 'AppleCare+' if warranty_info.repairs_and_service_capability =~ /AppleCare\+/
      warranty_info.repairs_and_service_kind = 'Limited Warranty' if warranty_info.repairs_and_service_capability =~ /Limited Warranty/

      warranty_info.eligible_for_extend_coverage = true if page.has_selector?('#app-true')
      warranty_info.eligible_for_extend_coverage = false if page.has_selector?('#app-false')
      warranty_info.eligible_for_extend_coverage_capability = find('#app-text').text if page.has_selector?('#app-text')

      warranty_info
    rescue Capybara::Poltergeist::StatusFailError # Bad Proxy or Interrupted Internet connection
      capture_warranty(identity, proxies)
    end
  end
end
