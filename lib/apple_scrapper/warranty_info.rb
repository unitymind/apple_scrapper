module AppleScrapper
  WarrantyInfo = Struct.new(
    :product_name, :validated_purchase,
    :phone_support_active, :phone_support_capability, :phone_support_kind, :phone_support_expiration_date,
    :repairs_and_service_active, :repairs_and_service_capability, :repairs_and_service_kind, :repairs_and_service_expiration_date,
    :eligible_for_extend_coverage, :eligible_for_extend_coverage_capability
  )
end
