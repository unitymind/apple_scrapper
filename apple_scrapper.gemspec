# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'apple_scrapper/version'

Gem::Specification.new do |spec|
  spec.name          = 'apple_scrapper'
  spec.version       = AppleScrapper::VERSION
  spec.authors       = ['Vitaliy V. Shopov']
  spec.email         = ['vitaliy.shopov@cleawing.com']

  spec.summary       = %q{Scrap warranty state and expired date using Apple by IMEI/Serial}
  spec.description   = %q{Scrap warranty state and expired date using Apple by IMEI/Serial}
  spec.homepage      = 'https://bitbucket.org/unitymind/apple_scrapper'
  spec.license       = 'MIT'

  spec.files         = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  spec.bindir        = 'bin'
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  spec.add_runtime_dependency     'capybara', '~> 2.4.4'
  spec.add_runtime_dependency     'phantomjs', '~> 1.9.8.0'
  spec.add_runtime_dependency     'poltergeist', '~> 1.6.0'
  spec.add_development_dependency 'bundler', '~> 1.9'
  spec.add_development_dependency 'rake', '~> 10.0'
  spec.add_development_dependency 'rspec', '~> 3.2.0'
end
