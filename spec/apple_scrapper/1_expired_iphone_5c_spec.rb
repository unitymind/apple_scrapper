require 'spec_helper'

describe AppleScrapper do
  context 'Expired iPhone 5c (IMEI: 013896000639712)' do
    before(:all) do
      @warranty_info  = AppleScrapper.capture_warranty('013896000639712', PROXIES)
    end

    it 'Product name: iPhone 5c' do
      expect(@warranty_info.product_name).to eq 'iPhone 5c'
    end

    it 'Has validated purchase date' do
      expect(@warranty_info.validated_purchase).to be_truthy
    end

    it 'Telephone Technical Support: Expired' do
      expect(@warranty_info.phone_support_active).to be false
    end

    it 'Telephone Technical Support Estimated Expiration Date is no exist' do
      expect(@warranty_info.phone_support_expiration_date).to be_nil
    end

    it 'Telephone Technical Support capability not empty' do
      expect(@warranty_info.phone_support_capability).to_not be_empty
    end

    it 'Telephone Technical Support kind: Apple Advisor' do
      expect(@warranty_info.phone_support_kind).to eq 'Apple Advisor'
    end

    it 'Repairs and Service Coverage: Expired' do
      expect(@warranty_info.repairs_and_service_active).to be false
    end

    it 'Repairs and Service Coverage Estimated Expiration Date is no exist' do
      expect(@warranty_info.repairs_and_service_expiration_date).to be_nil
    end

    it 'Repairs and Service Coverage capability not empty' do
      expect(@warranty_info.repairs_and_service_capability).to_not be_empty
    end

    it 'Repairs and Service Coverage kind: Not Covered' do
      expect(@warranty_info.repairs_and_service_kind).to eq 'Not Covered'
    end
  end
end