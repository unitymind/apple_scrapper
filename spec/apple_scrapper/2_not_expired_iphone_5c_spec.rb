require 'spec_helper'

describe AppleScrapper do
  context 'Not expired iPhone 5c under AppleCare+ (IMEI: 013977000323877)' do
    before(:all) do
      @warranty_info  = AppleScrapper.capture_warranty('013977000323877', PROXIES)
    end

    it 'Product name: iPhone 5c' do
      expect(@warranty_info.product_name).to eq 'iPhone 5c'
    end

    it 'Has validated purchase date' do
      expect(@warranty_info.validated_purchase).to be_truthy
    end

    it 'Telephone Technical Support: Active' do
      expect(@warranty_info.phone_support_active).to be_truthy
    end

    it 'Telephone Technical Support capability not empty' do
      expect(@warranty_info.phone_support_capability).to_not be_empty
    end

    it 'Telephone Technical Support kind: AppleCare+' do
      expect(@warranty_info.phone_support_kind).to eq 'AppleCare+'
    end

    it 'Telephone Technical Support Estimated Expiration Date: 2016-08-10' do
      expect(@warranty_info.phone_support_expiration_date).to eq Date.new(2016, 8, 10)
    end

    it 'Repairs and Service Coverage: Active' do
      expect(@warranty_info.repairs_and_service_active).to be_truthy
    end

    it 'Repairs and Service Coverage capability not empty' do
      expect(@warranty_info.repairs_and_service_capability).to_not be_empty
    end

    it 'Repairs and Service Coverage kind: AppleCare+' do
      expect(@warranty_info.repairs_and_service_kind).to eq 'AppleCare+'
    end

    it 'Repairs and Service Coverage Estimated Expiration Date: 2016-08-10' do
      expect(@warranty_info.repairs_and_service_expiration_date).to eq Date.new(2016, 8, 10)
    end
  end
end