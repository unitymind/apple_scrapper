require 'spec_helper'

describe AppleScrapper do
  context 'Checking invalid serial/imei' do
    it 'Empty value' do
      expect { AppleScrapper.capture_warranty('', PROXIES) }.to raise_error(AppleScrapper::InvalidSerialNumber, "Please enter your product's serial number.")
    end

    it 'Short invalid serial' do
      expect { AppleScrapper.capture_warranty('013', PROXIES) }.to raise_error(AppleScrapper::InvalidSerialNumber, 'Please enter a valid serial number.')
    end

    it 'Invalid serial' do
      expect { AppleScrapper.capture_warranty('01397700032387', PROXIES) }.to raise_error(
        AppleScrapper::InvalidSerialNumber, "We're sorry, but this serial number is not valid. Please check your information and try again."
      )
    end
  end
end