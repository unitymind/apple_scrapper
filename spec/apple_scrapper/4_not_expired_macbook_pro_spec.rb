require 'spec_helper'

describe AppleScrapper do
  context 'Not expired MacBook Pro under Limited Warranty (serial: C02NLP5TG3QH)' do
    before(:all) do
      @warranty_info  = AppleScrapper.capture_warranty('C02NLP5TG3QH', PROXIES)
    end

    it 'Product name: MacBook Pro (Retina, 13-inch, Mid 2014)' do
      expect(@warranty_info.product_name).to eq 'MacBook Pro (Retina, 13-inch, Mid 2014)'
    end

    it 'Has validated purchase date' do
      expect(@warranty_info.validated_purchase).to be_truthy
    end

    it 'Telephone Technical Support: Expired' do
      expect(@warranty_info.phone_support_active).to be false
    end

    it 'Telephone Technical Support capability not empty' do
      expect(@warranty_info.phone_support_capability).to_not be_empty
    end

    it 'Telephone Technical Support kind: Apple Advisor' do
      expect(@warranty_info.phone_support_kind).to eq 'Apple Advisor'
    end

    it 'Telephone Technical Support Estimated Expiration Date is no exist' do
      expect(@warranty_info.phone_support_expiration_date).to be_nil
    end

    it 'Repairs and Service Coverage: Active' do
      expect(@warranty_info.repairs_and_service_active).to be true
    end

    it 'Repairs and Service Coverage capability not empty' do
      expect(@warranty_info.repairs_and_service_capability).to_not be_empty
    end

    it 'Repairs and Service Coverage kind: Limited Warranty' do
      expect(@warranty_info.repairs_and_service_kind).to eq 'Limited Warranty'
    end

    it 'Repairs and Service Coverage Estimated Expiration Date: 2015-11-22' do
      expect(@warranty_info.repairs_and_service_expiration_date).to eq Date.new(2015, 11, 22)
    end

    it 'Eligible for the AppleCare Protection Plan: Yes' do
      expect(@warranty_info.eligible_for_extend_coverage).to be_truthy
    end

    it 'Eligible for the AppleCare Protection Plan capability not empty' do
      expect(@warranty_info.eligible_for_extend_coverage_capability).to_not be_empty
    end
  end
end